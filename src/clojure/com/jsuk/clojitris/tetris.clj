(def block { :I [[0 0] [1 0] [-1 0] [-2 0]]
             :O [[0 0] [1 0] [1 1] [0 1]]
             :T [[0 0] [0 1] [1 0] [-1 0]]
             :J [[0 0] [-2 0] [-1 0] [0 1]]
             :L [[0 0] [0 1] [1 0] [2 0]]
             :S [[0 0] [1 0] [0 1] [-1 1]]
             :Z [[0 0] [-1 0] [0 1] [1 1]]
             })
(defn rotate  [[x y]] [y (- x)])
(defn left [[x y]] [(- x 1) y])
(defn right [[x y]] [(+ x 1) y])
(defn down [[x y]] [x (+ y 1)])
(defn translate [[dx dy]] (fn [[x y]] [(+ x dx) (+ y dy)]))
(def field (into [] (repeat 10 (into [] (repeat 20 0)))))
; (assoc field x  (update-in (field x) [y] inc))
(print I)
